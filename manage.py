from fastapi import FastAPI
from app.routes import clients, users, stores, deliverys, couriers

app = FastAPI()

app.include_router(users.router)
app.include_router(clients.router)
app.include_router(stores.router)
app.include_router(deliverys.router)
app.include_router(couriers.router)
