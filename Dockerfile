FROM python:3.10-alpine
WORKDIR /app
RUN apk add --no-cache gcc musl-dev
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8000
CMD ["uvicorn", "manage:app", "--host", "0.0.0.0", "--port", "8000"]
