import datetime
from pydantic import BaseModel

class User(BaseModel):
    id: int
    name: str
    email: str

class Client(BaseModel):
    id: int
    name: str
    contact_name: str
    phone: str

class Store(BaseModel):
    id: int
    name: str
    location: str
    manager_id: int

class Delivery(BaseModel):
    id: int
    store_id: int
    courier_id: int
    delivery_address: str
    delivery_time: datetime.datetime

class Courier(BaseModel):
    id: int
    name: str
    phone: str
    vehicle_type: str 
