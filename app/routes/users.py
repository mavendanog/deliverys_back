from fastapi import APIRouter
from app.models import User

router = APIRouter()

@router.post("/users/", response_model=User, tags=["Users"], summary="Create a new user", description="Creates a new user with the given details and returns the user object.")
async def create_user(user: User):
    return user

@router.get("/users/{user_id}", response_model=User, tags=["Users"], summary="Read user by ID", description="Fetches a user by their unique identifier and returns the user object.")
async def read_user(user_id: int):
    return {"id": user_id, "name": "John Doe", "email": "john@example.com"}

@router.put("/users/{user_id}", response_model=User, tags=["Users"], summary="Update user by ID", description="Updates the details of an existing user identified by their unique identifier and returns the updated user object.")
async def update_user(user_id: int, user: User):
    return user

@router.delete("/users/{user_id}", tags=["Users"], summary="Delete user by ID", description="Deletes the user identified by their unique identifier and confirms the deletion.")
async def delete_user(user_id: int):
    return {"message": "User deleted"}
