from fastapi import APIRouter
from app.models import User

router = APIRouter()

@router.post("/couriers/", response_model=User, tags=["Couriers"], summary="Create a new courier", description="Creates a new courier with the given details and returns the user object.")
async def create_user(user: User):
    return user

@router.get("/couriers/{courier_id}", response_model=User, tags=["Couriers"], summary="Read courier by ID", description="Fetches a courier by their unique identifier and returns the user object.")
async def read_user(courier_id: int):
    return {"id": courier_id, "name": "John Doe", "email": "john@example.com"}

@router.put("/couriers/{courier_id}", response_model=User, tags=["Couriers"], summary="Update courier by ID", description="Updates the details of an existing courier identified by their unique identifier and returns the updated user object.")
async def update_user(courier_id: int, user: User):
    return user

@router.delete("/couriers/{courier_id}", tags=["Couriers"], summary="Delete courier by ID", description="Deletes the courier identified by their unique identifier and confirms the deletion.")
async def delete_user(courier_id: int):
    return {"message": "courier deleted"}
