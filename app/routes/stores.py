from fastapi import APIRouter
from app.models import User

router = APIRouter()

@router.post("/stores/", response_model=User, tags=["Stores"], summary="Create a new store", description="Creates a new store with the given details and returns the user object.")
async def create_user(user: User):
    return user

@router.get("/stores/{store_id}", response_model=User, tags=["Stores"], summary="Read store by ID", description="Fetches a store by their unique identifier and returns the user object.")
async def read_user(store_id: int):
    return {"id": store_id, "name": "John Doe", "email": "john@example.com"}

@router.put("/stores/{store_id}", response_model=User, tags=["Stores"], summary="Update store by ID", description="Updates the details of an existing user identified by their unique identifier and returns the updated user object.")
async def update_user(store_id: int, user: User):
    return user

@router.delete("/stores/{store_id}", tags=["Stores"], summary="Delete store by ID", description="Deletes the store identified by their unique identifier and confirms the deletion.")
async def delete_user(store_id: int):
    return {"message": "store deleted"}
