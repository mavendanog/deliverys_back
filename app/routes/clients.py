from fastapi import APIRouter
from app.models import User

router = APIRouter()

@router.post("/clients/", response_model=User, tags=["Clients"], summary="Create a new client", description="Creates a new client with the given details and returns the user object.")
async def create_user(user: User):
    return user

@router.get("/clients/{client_id}", response_model=User, tags=["Clients"], summary="Read client by ID", description="Fetches a client by their unique identifier and returns the user object.")
async def read_user(client_id: int):
    return {"id": client_id, "name": "John Doe", "email": "john@example.com"}

@router.put("/clients/{client_id}", response_model=User, tags=["Clients"], summary="Update client by ID", description="Updates the details of an existing client identified by their unique identifier and returns the updated user object.")
async def update_user(client_id: int, user: User):
    return user

@router.delete("/clients/{client_id}", tags=["Clients"], summary="Delete client by ID", description="Deletes the client identified by their unique identifier and confirms the deletion.")
async def delete_user(client_id: int):
    return {"message": "client deleted"}
