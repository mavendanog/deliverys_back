from fastapi import APIRouter
from app.models import User

router = APIRouter()

@router.post("/deliverys/", response_model=User, tags=["Deliverys"], summary="Create a new delivery", description="Creates a new delivery with the given details and returns the user object.")
async def create_user(user: User):
    return user

@router.get("/deliverys/{delivery_id}", response_model=User, tags=["Deliverys"], summary="Read delivery by ID", description="Fetches a delivery by their unique identifier and returns the user object.")
async def read_user(delivery_id: int):
    return {"id": delivery_id, "name": "John Doe", "email": "john@example.com"}

@router.put("/deliverys/{delivery_id}", response_model=User, tags=["Deliverys"], summary="Update delivery by ID", description="Updates the details of an existing delivery identified by their unique identifier and returns the updated user object.")
async def update_user(delivery_id: int, user: User):
    return user

@router.delete("/deliverys/{delivery_id}", tags=["Deliverys"], summary="Delete delivery by ID", description="Deletes the delivery identified by their unique identifier and confirms the deletion.")
async def delete_user(delivery_id: int):
    return {"message": "delivery deleted"}
