import os
from motor.motor_asyncio import AsyncIOMotorClient
from dotenv import load_dotenv

load_dotenv()

class DataBase:
    client: AsyncIOMotorClient = None

    @classmethod
    def initialize(cls):
        cls.client = AsyncIOMotorClient(os.getenv('MONGO_DETAILS'))

    @classmethod
    def get_collection(cls, collection_name):
        return cls.client.your_database_name[collection_name]

DataBase.initialize()
